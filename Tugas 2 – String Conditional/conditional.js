//Soal Nomor 1
var nama    = "Way";
var peran   = "Werewolf";

if(nama === ""){
    console.log("Nama harus diisi!");
}else if(peran === ""){
        console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
        }else if(nama !== "" && peran !== ""){
            if(peran === "Penyihir"){
                console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
                console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
                }else if(peran === "Guard"){
                    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
                    console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`);
                    }else if(peran === "Werewolf"){
                        console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
                        console.log(`Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!`);
            }
}

//SOAL NOMOR 2
var hari = 21; 
var bulan = 1; 
var tahun = 1945;

if(hari > 0 && hari <= 31){
    if(bulan > 0 && bulan <= 12){
        if(tahun >= 1900 && tahun <= 2200){
            switch(bulan){
                case 1: {
                    bulan = String("Januari");
                    break;
                }
                case 2:{
                    bulan = String("Februari");
                    break;
                }
                case 3:{
                    bulan = String("Maret");
                    break;
                }
                case 4:{
                    bulan = String("April");
                    break;
                }
                case 5:{
                    bulan = String("Mei");
                    break;
                }
                case 6:{
                    bulan = String("Juni");
                    break;
                }
                case 7:{
                    bulan = String("Juli");
                    break;
                }
                case 8:{
                    bulan = String("Agustus");
                    break;
                }
                case 9:{
                    bulan = String("September");
                    break;
                }
                case 10:{
                    bulan = String("Oktober");
                    break;
                }
                case 11:{
                    bulan = String("November");
                    break;
                }
                case 12:{
                    bulan = String("Desember");
                    break;
                }
            }
            console.log(`${hari} ${bulan} ${tahun}`);
        }else{
            console.log("Tahun hanya bisa dimasukan dari 1990-2200");    
        }
    }else{
    console.log("Bulan hanya ada 1-12");    
    }
}else{
    console.log("Tanggal hanya ada 1-31");
}