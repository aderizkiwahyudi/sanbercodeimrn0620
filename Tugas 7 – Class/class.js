//SOAL NOMOR 1
class Animal {
    constructor(name){
        this.name = name;
        this.legs = 4;
        this.cold_bloded = false;
    }
}

var sheep = new Animal("Shaun");

console.log("SOAL NOMOR 1");
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_bloded);

//SOAL NOMOR 2
class Ape extends Animal {
    constructor(name){
        super(name);
        this.legs = 2;
        this.yell = "Auoooo"
    }
    yel(){
        return this.yell;
    }
}

class Frog extends Animal{
    constructor(name){
        super(name);
        this.jum = "Hop Hop";
    }
    jump(){
        console.log(this.jum)
        return;
    }
}

var sunggokong = new Ape("Kera Sakti");

console.log("\n");
console.log("SOAL NOMOR 2");
console.log(sunggokong.name);
console.log(sunggokong.legs);
console.log(sunggokong.yel());

var kodok = new Frog("Buduk");
console.log(kodok.name);
console.log(kodok.legs);
kodok.jump();

//SOAL NOMOR 3;
class Clock {
    constructor({template}){
        this.template = template;
        this.timer    = '';
    }
    render(){
        this.date     = new Date();

        this.hours   = this.date.getHours();
        if(this.hours < 10) this.hours = "0"+this.hours;
        this.minute  = this.date.getMinutes();
        if(this.minute < 10) this.minute = "0"+this.minute;
        this.second  = this.date.getSeconds();
        if(this.second < 10) this.second = "0"+this.second;

        this.output = this.template
                      .replace('h', this.hours)
                      .replace('m', this.minute)
                      .replace('s', this.second)
                      
        console.log(this.output);
    }
    stop(){
        clearInterval(timer);
    }
    start(){
        console.log("\n");
        console.log("SOAL NOMOR 3");
        this.render();
        setInterval(this.render.bind(this),1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  