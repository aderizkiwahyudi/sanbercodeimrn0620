//SOAL NOMOR 1
function teriak(){
    console.log('SOAL NOMOR 1\n===========================================');
    console.log('Halo Sanbers!');
}
teriak();

//SOAL NOMOR 2
function kalikan(a,b){
    return a*b;
}

num1 = 5;
num2 = 10;
var hasilKali = kalikan(num1,num2);
console.log('\nSOAL NOMOR 2\n===========================================','\n'+hasilKali);

//SOAL NOMOR 3
function introduce(name, age, address, hobby){
    return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`;
}

var name = "Ade Rizki Wahyudi";
var age = 18;
var address = "Perumahan Sarjana Resident Indralaya, Sumatra Selatan";
var hobby = "Coding";
 
var perkenalan = introduce(name, age, address, hobby);
console.log('\nSOAL NOMOR 3\n===========================================','\n'+perkenalan);