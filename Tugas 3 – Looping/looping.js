//SOAL NOMOR 1
var i = 1;
console.log('SOAL NO 1','\nLOOPING PERTAMA');
while(i <= 10){
    console.log(i*2,' - I Love Coding');
    i++;
}
var ii = 10;
console.log('LOOPING KEDUA');
while(ii >= 1){
    console.log(ii*2,' - I Love Coding');
    ii--;
}

//SOAL NOMOR 2
console.log('\nSOAL NO 2');
for(var no=1; no<=20; no++){
    if(no%2 == 0){
        console.log(no,' - Berkualitas');
    }else if(no%3 == 0){
            console.log(no,' - I Love Coding');
    }else{
        console.log(no,' - Santai')
    }
}

//SOAL NOMOR 3
console.log('\nSOAL NO 3');
var persegi = 1;
var per = '#';
while(persegi <=4){
    for(var iper = 1; iper < 8; iper++){
        per += '#';
    }
    console.log(per);
    persegi++;
    per = '#';
}

//SOAL NOMOR 4
console.log('\nSOAL NO 4');
var tangga = 0;
var tan = '';
while(tangga < 7){
    tan += '#';
    console.log(tan);
    tangga ++;
}

//SOAL NOMOR 5
console.log('\nSOAL NO 5');
var hasil = '';
for(var catur = 0; catur < 8; catur++){
    if(catur%2 == 0){
        for(var c = 0; c<4; c++){
            hasil = ' #'+hasil;
        }
    }else{
        for(var cc = 0; cc<4; cc++){
            hasil = '# '+hasil;
        }
    }
    console.log(hasil);
    hasil = '';
}