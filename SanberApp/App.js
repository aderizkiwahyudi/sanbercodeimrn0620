import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
//import Tugas from './Tugas/Tugas12/App.js';
//import Tugas13 from './Tugas/Tugas13/LoginScreen.js';
//import Tugas13 from './Tugas/Tugas13/RegisterScreen.js';
//import AboutScreen from './Tugas/Tugas13/AboutScreen.js';
//import SkillScreen from "./Tugas/Tugas14/SkillScreen.js"; 
//import Main from "./Tugas/Tugas14/App.js";
//import Main from "./Tugas/Tugas15/index.js";
//import Main from "./Tugas/TugasNavigation/index.js";
import Main from "./Tugas/Quiz3/index.js";


export default function App() {
  return (
    <Main/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
