import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Text,View,TouchableOpacity,StyleSheet,ScrollView} from 'react-native';

export default class App extends Component {
    render(){
        return(
            <ScrollView>
            <View style={styles.container}>
                <View style={styles.body}>
                    <Text style={styles.about}>Tentang Saya</Text>
                    <Icon name="user-circle" size={150} style={{color:'#EFEFEF', marginVertical:20}}></Icon>
                    <Text style={styles.nama}>Ade Rizki Wahyudi</Text>
                    <Text style={{color:'#3EC6FF'}}>React Native Developer</Text>
                    <View style={styles.box}>
                        <Text style={styles.boxTitle}>Portofolio</Text>
                        <View style={styles.portofolio}>
                            <View style={styles.portofolioIcon}>
                                <Icon name="gitlab" size={50} style={{color:'#3EC6FF'}}></Icon>
                                <Text style={{color : '#003366'}}>@aderizkiwahyudi</Text>
                            </View>
                            <View style={styles.portofolioIcon}>
                                <Icon name="github" size={50} style={{color:'#3EC6FF'}}></Icon>
                                <Text style={{color : '#003366'}}>@aderizkiwahyudi</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.box}>
                        <Text style={styles.boxTitle}>Hubungi Saya</Text>
                        <View style={styles.hubungiSaya}>
                            <View style={styles.hubungiSayaIcon}>
                                <Icon name="facebook-square" size={50} style={{color:'#3EC6FF'}}></Icon>
                                <Text style={{color : '#003366', marginVertical : 15, marginLeft: 15}}>aderizkiwahyudi</Text>
                            </View>
                            <View style={styles.hubungiSayaIcon}>
                                <Icon name="instagram" size={50} style={{color:'#3EC6FF'}}></Icon>
                                <Text style={{color : '#003366', marginVertical : 15, marginLeft: 15}}>@aderizkiwahyudi</Text>
                            </View>
                            <View style={styles.hubungiSayaIcon}>
                                <Icon name="twitter" size={50} style={{color:'#3EC6FF'}}></Icon>
                                <Text style={{color : '#003366', marginVertical : 15, marginLeft: 15}}>@aderizkiwahyudi</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        marginVertical: 50,
    },
    body : {
        flexDirection : 'column',
        alignContent : 'center',
        alignItems : 'center',
        marginVertical : 20
    },
    about : {
        fontSize : 25,
        fontWeight : 'bold',
        color  : '#003366'
    },
    nama : {
        fontSize : 20,
        fontWeight : 'bold',
        color : '#003366'
    },
    box : {
        marginVertical : 20,
        width : '80%',
        backgroundColor : '#EFEFEF',
        padding : 20,
        borderRadius : 5
    },
    boxTitle : {
        color : '#003366',
        paddingBottom : 10,
        borderBottomWidth : 1,
        width : '100%',
        borderColor : '#003366',
        fontWeight : 'bold'
    },
    portofolio : {
        flexDirection : 'row',
        alignItems : 'center',
        alignContent : 'center',
        marginVertical : 10,
    },
    portofolioIcon : {
        alignItems : 'center',
        alignContent : 'center',
        paddingHorizontal : 5,
    },
    hubungiSaya : {
        flexDirection: 'column',
        alignContent : 'center',
        alignItems : 'center',
        paddingVertical : 10
    },
    hubungiSayaIcon : {
        flexDirection : 'row',
        marginRight : 10,
        marginVertical : 10
    }
})