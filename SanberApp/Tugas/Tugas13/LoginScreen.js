import React, {Component} from 'react';
import {View,Text,Image,TextInput,TouchableOpacity,StyleSheet} from 'react-native';

export default class App extends Component {
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image source={require('./img/logo.png')} style={styles.logo}></Image>
                </View>
                <View style={styles.body}>
                    <Text style={styles.textRegister}>Login</Text>
                    <View style={styles.containerInput}>
                        <Text style={styles.labelInput}>Username/Email</Text>
                        <TextInput style={styles.Input}></TextInput>
                        <Text style={styles.labelInput}>Password</Text>
                        <TextInput secureTextEntry={true} style={styles.Input}></TextInput>
                    </View>
                </View>
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.btnMasuk}>
                        <Text style={{color:'#fff'}}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={{color:'#3EC6FF', marginVertical:10}}>atau</Text>
                    <TouchableOpacity style={styles.btnDaftar}>
                        <Text style={{color:'#fff'}}>Daftar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex:1
    },
    header : {
        flexDirection:'row',
        backgroundColor:'white',
        justifyContent: 'space-around',
        marginTop : 75
    },
    logo : {
        width:250,
        height:70,
    },
    body : {
        backgroundColor:'white',
        flexDirection: 'column',
        justifyContent:'space-around',
        textAlign:'center',
        marginTop : 50
    },
    textRegister : {
        fontSize : 20,
        color: '#003366',
        textAlign:'center'
    },
    containerInput : {
        flexDirection : 'column',
        paddingTop : 20,
        paddingHorizontal : 50,
        backgroundColor : 'white',
    },
    labelInput : {
        textAlign : 'left',
        color : '#003366',
        paddingBottom : 5
    },
    Input : {
        borderWidth : 1,
        borderColor : '#003366',
        paddingHorizontal : 15,
        paddingVertical : 5,
        marginBottom : 15
    },
    footer : {
        backgroundColor : 'white',
        flexDirection : 'column',
        alignItems : 'center',
        marginVertical: 20
    },
    btnMasuk : {
        backgroundColor : '#3EC6FF',
        borderRadius : 50,
        paddingHorizontal : 40,
        paddingVertical : 10
    },
    btnDaftar : {
        backgroundColor : '#003366',
        borderRadius : 50,
        paddingHorizontal : 40,
        paddingVertical : 10
    }
});