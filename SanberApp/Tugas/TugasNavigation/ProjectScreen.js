import React,{Component} from "react";
import {View,Text,TouchableOpacity,StyleSheet,Image, ScrollView, FlatList} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import data from "./skillData.json";
import Item from "./skillItem.js";

export const Project = ({navigation}) => (
    
            <View style={styles.container}>
                <View style={{ margin: 20, textAlign:'center',flexDirection:'column', justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                    <Text>Halaman Proyek</Text>
                </View>
            </View>
)

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor:'#fff'
    },
    header : {
        flexDirection: 'column',
        alignContent:'flex-end',
        alignItems:'flex-end',
        backgroundColor:'white',
    },
    logo : {
        width: 200,
        height:70
    },
    profil : {
        flexDirection : 'row',
        alignContent : 'space-around'
    }, 
    userInfo : {
        marginHorizontal:10
    },
    body : {
        flexDirection : 'column',
        marginVertical:10,
    },
    bars : {
        flexDirection : 'row',
        alignContent : 'center',
        justifyContent:'space-around'
    },
    btnBars : {
        alignContent : 'center',
        alignItems : "center",
        backgroundColor : '#B4E9FF',
        padding: 10,
        margin:10,
        borderRadius:50
    },
    menu : {
        flexDirection : 'column',
        marginVertical : 10,
    },
    skil : {
        flexDirection : 'row',
        backgroundColor : '#B4E9FF',
        padding : 10,
        borderRadius:10,
        marginBottom : 20
    },
    skilInfo : {
        flexDirection : 'column',
        marginHorizontal : 10,
        width:'40%'
    }
})