import React,{Component} from "react";
import {View,Text,TouchableOpacity,StyleSheet,Image, ScrollView} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class App extends Component {
    render(){
        let data = this.props.data;
        return(
            <View style={styles.menu}>
                 <TouchableOpacity style={styles.skil}>
                     <Icon name={data.iconName} size={80} style={{color:'#003366'}}></Icon>
                    <View style={styles.skilInfo}>
                        <Text style={{color:'#003366', fontSize:20}}>{data.skillName}</Text>
                        <Text style={{color:'#5bcfff', fontSize:12}}>{data.categoryName}</Text>
                        <Text style={{color:'#fff', fontSize:20, textAlign:'right'}}>{data.percentageProgress}</Text>
                    </View>
                    <Icon name="chevron-right" size={80} style={{color:'#003366'}}></Icon>
                </TouchableOpacity>
            </View>          
        )
    }
}

const styles = StyleSheet.create({
    menu : {
        flexDirection : 'column',
    },
    skil : {
        flexDirection : 'row',
        backgroundColor : '#B4E9FF',
        padding : 10,
        borderRadius:10,
        marginBottom : 20
    },
    skilInfo : {
        flexDirection : 'column',
        marginHorizontal : 10,
        width:'40%'
    }
})