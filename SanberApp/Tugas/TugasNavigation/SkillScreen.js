import React,{Component} from "react";
import {View,Text,TouchableOpacity,StyleSheet,Image, ScrollView, FlatList} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import data from "./skillData.json";
import Item from "./skillItem.js";

export const Skill = ({navigation}) => (
    
            <View style={styles.container}>
                <View style={{ margin: 20}}>
                    <View style={styles.header}>
                        <Image source={require('./img/logo.png')} style={styles.logo}></Image>
                    </View>
                    <View style={styles.profil}>
                        <Icon name="account-circle" size={40} style={{color:'#B4E9FF'}}></Icon>
                        <View style={styles.userInfo}>
                            <Text style={{color:'#003366'}}>Hai,</Text>
                            <Text style={{color:'#003366'}}>Ade Rizki Wahyudi</Text>
                        </View>
                    </View>
                    <View style={styles.body}>
                        <Text style={{color:'#003366', fontSize:40, borderBottomWidth:3, borderColor:'#B4E9FF'}}>SKILL</Text>
                        <View style={styles.bars}>
                            <TouchableOpacity style={styles.btnBars}><Text style={{color:'#003366', fontSize:11}}>Library/Framework</Text></TouchableOpacity>
                            <TouchableOpacity style={styles.btnBars}><Text style={{color:'#003366', fontSize:11}}>Bahasa Pemrograman</Text></TouchableOpacity>
                            <TouchableOpacity style={styles.btnBars}><Text style={{color:'#003366', fontSize:11}}>Teknologi</Text></TouchableOpacity>
                        </View>
                            <FlatList
                            data={data.items}
                            renderItem={(skil) => <Item data={skil.item}/>}
                            keyExtractor={(item) => {item.id}}
                            style={{marginBottom:70, marginTop:20}}
                            >
                            </FlatList>
                    </View>
                </View>
            </View>
)

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor:'#fff'
    },
    header : {
        flexDirection: 'column',
        alignContent:'flex-end',
        alignItems:'flex-end',
        backgroundColor:'white',
    },
    logo : {
        width: 200,
        height:70
    },
    profil : {
        flexDirection : 'row',
        alignContent : 'space-around'
    }, 
    userInfo : {
        marginHorizontal:10
    },
    body : {
        flexDirection : 'column',
        marginVertical:10,
    },
    bars : {
        flexDirection : 'row',
        alignContent : 'center',
        justifyContent:'space-around'
    },
    btnBars : {
        alignContent : 'center',
        alignItems : "center",
        backgroundColor : '#B4E9FF',
        padding: 10,
        margin:10,
        borderRadius:50
    },
    menu : {
        flexDirection : 'column',
        marginVertical : 10,
    },
    skil : {
        flexDirection : 'row',
        backgroundColor : '#B4E9FF',
        padding : 10,
        borderRadius:10,
        marginBottom : 20
    },
    skilInfo : {
        flexDirection : 'column',
        marginHorizontal : 10,
        width:'40%'
    }
})