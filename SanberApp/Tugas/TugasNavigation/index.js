import React from 'react';
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {createDrawerNavigator} from "@react-navigation/drawer";

import {Login} from './LoginScreen.js';
import {Register} from './RegisterScreen.js';
import {About} from './AboutScreen.js';
import {Skill} from './SkillScreen.js';
import {Add} from './AddScreen.js';
import {Project} from './ProjectScreen.js';

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const LoginStack = createStackNavigator();
const Drawer = createDrawerNavigator();


const LoginStackScreen = () => (
    <LoginStack.Navigator>
      <LoginStack.Screen name="Login" component={Login}  options={{
        headerShown : false,
    }}/>
    <LoginStack.Screen name="Register" component={Register}  options={{
        headerShown : false,
    }}/>
    <LoginStack.Screen name="About" component={HomeStackScreen}  options={{
        headerShown : false,
    }}/>
    </LoginStack.Navigator>
  );

const HomeStackScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="About" component={AboutStackScreen}/>
    </Drawer.Navigator>  
)

const AboutStackScreen = () => (
    <AuthStack.Navigator>
        <AuthStack.Screen name="About" component={TabsScreenAbout}  options={{
        headerShown : false,
        }}/>
    </AuthStack.Navigator>
)

const TabsScreenAbout = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="About" component={About}></Tabs.Screen>
        <Tabs.Screen name="Skill" component={TabsScreenSkill}></Tabs.Screen>
        <Tabs.Screen name="Add" component={TabsScreenAdd}></Tabs.Screen>
        <Tabs.Screen name="Project" component={TabsScreenProject}></Tabs.Screen>
    </Tabs.Navigator>
)

const TabsScreenSkill = () => (
    <AuthStack.Navigator>
        <AuthStack.Screen name="Skill" component={Skill} ></AuthStack.Screen>
    </AuthStack.Navigator>
)
const TabsScreenAdd = () => (
    <AuthStack.Navigator>
        <AuthStack.Screen name="Add" component={Add} ></AuthStack.Screen>
    </AuthStack.Navigator>
)
const TabsScreenProject = () => (
    <AuthStack.Navigator>
        <AuthStack.Screen name="Project" component={Project} ></AuthStack.Screen>
    </AuthStack.Navigator>
)
export default function App() {
    return (
    <NavigationContainer>
        <AuthStack.Navigator>
            <AuthStack.Screen name="Login" component={LoginStackScreen} options={{
            headerShown : false,
            }}/>
        </AuthStack.Navigator>      
      </NavigationContainer>  
      );
  }

  