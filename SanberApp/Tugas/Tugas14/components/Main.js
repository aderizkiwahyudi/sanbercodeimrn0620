import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet, FlatList } from 'react-native';

import Note from './Note';

export default class Main extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            note : [],
            noteText : ''
        }
    }
    render(){

      let notes = this.state.note.map((val, key) => {
        return <Note key={key} keyval={key} val={val}
              deleteMethod={() => this.deleteNote(key)}
        ></Note>
      })

      return(
        <View style={styles.container}>
            <View style={styles.header}>
              <Text>WayNote</Text>
            </View>
            <ScrollView>
              {notes}
            </ScrollView>
            <View style={styles.footer}>
                <TextInput 
                style={styles.textInput}
                placeholder={"Masukan Note"}
                onChangeText={(noteText) => this.setState({noteText})}
                value={this.state.noteText}
                ></TextInput>
            </View>
            <TouchableOpacity onPress={this.addNote.bind(this)} style={styles.addButton}>
                <Text style={styles.addButtonText}>+</Text>
            </TouchableOpacity>
        </View>
      )
    }
    addNote(){
      if(this.state.noteText){
        let d = new Date();
        this.state.note.push({
          'date' : d.getDate() + "/" + d.getMonth() + "/" + d.getFullYear(),
          'note' : this.state.noteText
        });
        this.setState({ note : this.state.note })
        this.setState({ noteText : '' })
      }
    }

    deleteNote(key){
      this.state.note.splice(key, 1);
      this.setState({ note : this.state.note });
    }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#E91E63',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 10,
    borderBottomColor: '#ddd',
    marginTop:25,
    paddingVertical:20
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    padding: 26,
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 10,
  },
  textInput: {
    alignSelf: 'stretch',
    color: '#fff',
    padding: 20,
    backgroundColor: '#252525',
    borderTopWidth: 2,
    borderTopColor: '#ededed',
  },
  addButton: {
    position: 'absolute',
    zIndex: 11,
    right: 20,
    bottom: 90,
    backgroundColor: '#E91E63',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
});

