import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons.js';
import VideoItem from './component/VideoItem.js';
import data from './data.json';
export default class App extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <Image source={require('./images/logo.png')} style={{width:98, height:22}}></Image>
                    <View style={styles.navRight}>
                        <TouchableOpacity>
                        <Icon style={styles.navItem} name="search" size={25}></Icon> 
                        </TouchableOpacity>
                        <TouchableOpacity>
                        <Icon style={styles.navItem} name="account-circle" size={25}></Icon>
                        </TouchableOpacity>                
                    </View>
                </View>
                <View style={styles.body}>
                    <FlatList
                    data={data.items}
                    renderItem={(video) => <VideoItem video={video.item}/>}
                    keyExtractor={(item) => item.id}
                    ItemSeparatorComponent={() => <View style={{height:0.5,backgroundColor:'#e5e5e5'}}></View>}
                    ></FlatList>
                </View>
                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="home" size={20}></Icon>
                        <Text style={styles.tabTitle}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="whatshot" size={20}></Icon>
                        <Text style={styles.tabTitle}>Tranding</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="subscriptions" size={20}></Icon>
                        <Text style={styles.tabTitle}>Subscriptions</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="folder" size={20}></Icon>
                        <Text style={styles.tabTitle}>Library</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
} 

const styles = StyleSheet.create({
    container : {
        flex:1,
    },
    navBar : {
        height: 55,
        backgroundColor: '#fff',
        elevation:3,
        paddingHorizontal:15,
        flexDirection: 'row',
        alignItems:'center',
        justifyContent: 'space-between',
        marginTop:25
    },
    navRight : {
        flexDirection: 'row'
    },
    navItem : {
        marginLeft : 25
    },
    body : {
        flex : 1
    },
    tabBar : {
        backgroundColor : 'white',
        height : 60,
        borderTopWidth: 0.5,
        borderColor: '#e5e5e5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabItem : {
        alignItems : 'center',
        justifyContent : 'center'
    },
    tabTitle : {
        color:'#3c3c3c',
        textAlign:'center',
        paddingTop:2
    }
});

