var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
var waktu = 10000;
function call(i){
    var batas = waktu;
    if(i === books.length){
        return 0;
    }
    readBooksPromise(waktu,books[i])
        .then( (res) => {
            waktu = res;
            if(batas !== res){
                call(i+1);
            }
        })
        .catch( (err) => {
            console.log(err);
        });
}
call(0)