var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var waktu = 10000;
function call(i){
    var batas = waktu;
    if(i === books.length){
        return 0;
    }
    readBooks(waktu,books[i],(check) => {
        waktu = check;
        if(batas !== check){
            call(i+1);
        }
    })
}
call(0);