function range(startNum, finishNum) {
    let number = [];
    if(startNum !== undefined && finishNum !== undefined){   
        if(startNum < finishNum){
            while(startNum <= finishNum){
                number.push(startNum);
                startNum++;
            }
        }else{
            while(startNum >= finishNum){
                number.push(startNum);
                startNum--;
            }
        }
        return number; 
    }else{
        return -1;
    }
}

console.log("SOAL NOMOR 1");
console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());

//SOAL NOMOR 2
function rangeWithStep(startNum, finishNum, step) {
    let number = [];
    if(startNum !== undefined && finishNum !== undefined){   
        if(startNum < finishNum){
            while(startNum <= finishNum){
                number.push(startNum);
                startNum += step;
            }
        }else{
            while(startNum >= finishNum){
                number.push(startNum);
                startNum -= step;;
            }
        }
        return number; 
    }else{
        return -1;
    }
}
console.log("\n");
console.log("SOAL NOMOR 2");
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3)); 
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4)); 

//SOAL NOMOR 3
function sum(a,b,c){
    let hasil = 0;
    if(c === undefined){
        c = 1;
    }
    if(a === undefined){
        return 0;
    }else if(b === undefined){
        return a;
    }else{
        if(a <= b){
            while(a <= b){
                hasil += a;
                a += c;
            }
        }else{
            while(b <= a){
                hasil += b;
                b += c;
            }
        }
    return hasil;
    }
}
console.log("\n");
console.log("SOAL NOMOR 3");
console.log(sum(1,10));
console.log(sum(5,50,2));
console.log(sum(15,10));
console.log(sum(20,10,2)); 
console.log(sum(1));
console.log(sum());

//SOAL NOMOR 4
console.log("\n");
console.log("SOAL NOMOR 4");
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(){
    for(let i = 0; i < input.length; i++){
        console.log(`Nomor ID : ${input[i][0]}`);
        console.log(`Nama Lengkap : ${input[i][1]}`);
        console.log(`TTL : ${input[i][2]}, ${input[i][3]}`);
        console.log(`Hobi : ${input[i][4]}`);
        console.log(`\n`);
    }
    return;
}

dataHandling();

//NOMOR 5
function balikKata(txt){
    let kata = '';
    let i = txt.length - 1;
    while(i >= 0){
        kata += txt[i];
        i--;
    }
    return kata;
}
console.log("SOAL NOMOR 5");
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

//NOMOR 6
let database = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(database){
    let data = database;
    data.splice(data.length - 1,1,"Pria", "SMA Internasional Metro");

    let tanggal = data[3].split("/");
    let mm      = tanggal[1];
    let join    = tanggal.join("-");
    let desc    = tanggal.sort(function(a,b){return b-a;});
    let nama    = data[1].slice(0,14);
    console.log(data);
    console.log(bulan(parseInt(mm)));
    console.log(desc);
    console.log(join);
    console.log(nama);
    
}
function bulan(m){
    switch(m){
        case 01: {
            m = "Januari";
            break;
            }
        case 02:{
            m = "Februari"
            break;
        }
        case 03:{
            m = "Maret"
            break;
        }
        case 04:{
            m = "April"
            break;
        }
        case 05:{
            m = "Mei"
            break;
        }
        case 06:{
            m = "Juni"
            break;
        }
        case 07:{
            m = "Juli"
            break;
        }
        case 08:{
            m = "Agustus"
            break;
        }
        case 09:{
            m = "September"
            break;
        }
        case 10:{
            m = "Oktober"
            break;
        }
        case 11:{
            m = "November"
            break;
        }
        case 12:{
            m = "Desember"
            break;
        }
        
    }
    return m;
}

console.log("\n");
console.log("SOAL NOMOR 6");
dataHandling2(database);