//SOAL NOMOR 1
console.log("SOAL NOMOR 1");
function arrayToObject(obj){
        var date = new Date();
        var thn  = date.getFullYear();
        var data = [];
        if(obj.length !== 0){
            obj.forEach( _obj => {
                var age = thn - _obj[3];
                var hasil = "";
                if(_obj[3] === undefined || age < 0){
                    hasil = "Invailed birth Years"
                }else{
                    hasil = age.toString();
                }
                var fullname =  _obj[0]+" "+_obj[1]
                data.push(
                    {
                    fullname : fullname,    
                    bio : {
                    firstName   : _obj[0],
                    lastName    : _obj[1],
                    gander      : _obj[2],
                    age         : hasil
                }});
            });
            
            data.forEach(_data => {
                console.log();
                console.log(_data.fullname,":",_data.bio);
                return;
            });
        }else{
            console.log("");
            return;
        }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people); 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2);

arrayToObject([]);


//SOAL NOMOR 2
console.log("\n");
console.log("SOAL NOMOR 2");
var barang = [
    {
        nama  : "Sepatu Stacattu",
        harga : 1500000
    },
    {
        nama  : "Baju Zoro",
        harga : 500000
    },
    {
        nama  : "Baju H&N",
        harga : 250000
    },
    {
        nama  : "Swrater Uniklooh",
        harga : 175000
    },
    {
        nama  : "Casing Handphone",
        harga : 50000
    }
]
function shoppingTime(memberId, money) {
    var member = {
            memberId : memberId,
            money    : money,
            listPurchased : [],
            changeMoney : 0
        };
    if(memberId === "" || memberId === undefined){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }else{
        if(money <= 50000){
            return "Mohon maaf, uang tidak cukup";
        }else{
            let i = 0;
            var total_belanja = 0;
            barang.forEach( _barang => {
                if(money >= barang[i].harga){
                    member.listPurchased.push(barang[i].nama);
                    total_belanja += barang[i].harga;
                    member.changeMoney = money - total_belanja;
                }
                i++;
            });
            return member;
        }
    }
  }
   
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000)); 
  console.log(shoppingTime('234JdhweRxa53', 15000));
  console.log(shoppingTime()); 


  //SOAL NOMOR 3
console.log("\n");
console.log("SOAL NOMOR 3");
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var data_penumpang = [];
    arrPenumpang.forEach( _arrPenumpang =>{
        
        var from = rute.indexOf(_arrPenumpang[1]) + 1;
        var to   = rute.indexOf(_arrPenumpang[2]) + 1;
        
        console.log(from,to);

        var _bayar = (to-from)*2000;
        
        data_penumpang.push(
            {
                penumpang : _arrPenumpang[0],
                naikDari  : _arrPenumpang[1],
                tujuan    : _arrPenumpang[2],
                bayar     : _bayar
            }
        );

    });

    return data_penumpang;
  }
   
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  console.log(naikAngkot([])); //[]